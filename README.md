# My Alpine APKBUILDS

This repository contains my APKBUILD files for compiling different
packages for Alpine Linux.

* **keepassx1** -- an older version of KeePassX, which still uses `.kdb`
  databases as its primary format instead of `.kdbx`.

* **qpdfview** -- a Qt-based document viewer. Supports many formats, but
  this specific build only enables PDF, as libraries for other formats
  have not been packaged yet.

* **xfonts-bolkhov-git** -- a slightly patched version of the Cyr-RFX
  bitmap fonts for X11.

In addition, a bunch of packages from the **edge** repo are backported to be
buildable on the stable distro.
