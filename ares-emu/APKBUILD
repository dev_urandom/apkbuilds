# Maintainer: /dev/urandom <dev.urandom@posteo.org>
# Based on the AUR package by Snowstorm64

pkgname=ares-emu
pkgver=133
pkgrel=0
pkgdesc="Multi-system emulator by Near with experimental Nintendo 64 and PlayStation support"
arch="x86_64 i686"
url="https://ares-emu.net/"
license="ISC"
depends="gtk+3.0 gtksourceview libao mesa-gl libpulse eudev-libs libxv openal-soft sdl2 vulkan-loader"
makedepends="mesa git xorgproto gtk+3.0-dev gtksourceview-dev libao-dev mesa-dev pulseaudio-dev eudev-dev libxv-dev openal-soft-dev sdl2-dev"
source="${pkgname}-v${pkgver}.tar.gz::https://github.com/higan-emu/ares/archive/refs/tags/v${pkgver}.tar.gz
        ares-paths.patch"

prepare() {
  # Replace the placeholder with pkgver to automatically point at the source folder
  sed -i "s/PLACEHOLDER/${pkgver}/g" "${srcdir}/ares-paths.patch"

  # Patch Ares so that it can look for its files that are installed system-wide here
  patch -Np1 -i "${srcdir}/ares-paths.patch"
}

build() {
  make -C "${srcdir}/ares-${pkgver}/desktop-ui" hiro=gtk3
}

package() {
  install -Dm 644 "${srcdir}/ares-${pkgver}/LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  install -Dm 755 "${srcdir}/ares-${pkgver}/desktop-ui/out/ares" -t "${pkgdir}/usr/bin/"
  install -Dm 644 "${srcdir}/ares-${pkgver}/desktop-ui/resource/ares.png" -t "${pkgdir}/usr/share/icons/hicolor/256x256/apps/"
  install -Dm 644 "${srcdir}/ares-${pkgver}/desktop-ui/resource/ares.desktop" -t "${pkgdir}/usr/share/applications/"

  # Also install the shaders in Ares' shared directory
  install -dm 755 "${pkgdir}/usr/share/ares"
  cp -dr "${srcdir}/ares-${pkgver}/ares/Shaders/" "${pkgdir}/usr/share/ares/Shaders/"
}

sha512sums="
46a10c57b2727408058860de52d348b4a4716f218b5d517d88a9d20af8d60625e555216545889972052b8a85f0f8cd49a26ff93223f31ba3b6ba2ce53c496230  ares-emu-v133.tar.gz
3656beb37c4a92bdb848a9fc6a17285352729d92baaf1c46e040429579c365e386f0e79e723d610b3934b7dc64624118822671fad1518e6b95c9b45d0885ab25  ares-paths.patch
"
